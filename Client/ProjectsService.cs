﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using LINQ.Model;
using Newtonsoft.Json;
using Task = LINQ.Model.Task;
using LINQ.Model.SpecialClasses;

namespace LINQ
{
    class ProjectsService
    {
        private static HttpClient Client = new HttpClient();
        
        private readonly string url;
        
        public ProjectsService(string apiUrl)
        {
            url = apiUrl + "/api/";
        }

        #region RemoveData
        public void Remove(string dataType, int id)
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Delete, url + dataType + "/" + id);
                Client.SendAsync(request);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine($"The error was caught:\n {e.Message}");
            }
        }
        #endregion

        #region GetData
        public IEnumerable<string> GetMessages()
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, url + "messages/");
                HttpResponseMessage response = Client.SendAsync(request).Result;
                response.EnsureSuccessStatusCode();

                string responseBody = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<IEnumerable<string>>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine($"The error was caught:\n {e.Message}");
                return new List<string>();
            }
        }
        public IEnumerable<State> GetStates()
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, url + "state/");
                HttpResponseMessage response = Client.SendAsync(request).Result;
                response.EnsureSuccessStatusCode();

                string responseBody = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<IEnumerable<State>>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine($"The error was caught:\n {e.Message}");
                return new List<State>();
            }
        }
        public IEnumerable<Project> GetProjects()
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, url + "project/");
                HttpResponseMessage response = Client.SendAsync(request).Result;
                response.EnsureSuccessStatusCode();

                string responseBody = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<IEnumerable<Project>>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine($"The error was caught:\n {e.Message}");
                return new List<Project>();
            }
        }
        public IEnumerable<Team> GetTeams()
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, url + "team/");
                HttpResponseMessage response = Client.SendAsync(request).Result;
                response.EnsureSuccessStatusCode();

                string responseBody = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<IEnumerable<Team>>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine($"The error was caught:\n {e.Message}");
                return new List<Team>();
            }
        }
        public IEnumerable<User> GetUsers()
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, url + "user/");
                HttpResponseMessage response = Client.SendAsync(request).Result;
                response.EnsureSuccessStatusCode();

                string responseBody = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<IEnumerable<User>>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine($"The error was caught:\n {e.Message}");
                return new List<User>();
            }
        }
        public IEnumerable<Task> GetTasks()
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, url + "task/");
                HttpResponseMessage response = Client.SendAsync(request).Result;
                response.EnsureSuccessStatusCode();

                string responseBody = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<IEnumerable<Task>>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine($"The error was caught:\n {e.Message}");
                return new List<Task>();
            }
        }
        #endregion

        #region AddUpdateData
        public void AddData(string dataType, dynamic data)
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, url + dataType);
                var content = JsonConvert.SerializeObject(data);
                request.Content = new StringContent(content, Encoding.UTF8, "application/json");
                var result = Client.SendAsync(request).Result;
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine($"Smth get's wrong {e.Message}");
            }
        }
        public void UpdateData(string dataType, dynamic data)
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Put, url + dataType);
                var content = JsonConvert.SerializeObject(data);
                request.Content = new StringContent(content, Encoding.UTF8, "application/json");
                var result = Client.SendAsync(request).Result;
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine($"Smth get's wrong {e.Message}");
            }
        }
        #endregion

        #region LINQRequests
        public Dictionary<Project, int> GetProjectTasksCountByUserId(int id)
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, url + "projectTasks/" + id);
                HttpResponseMessage response = Client.SendAsync(request).Result;
                response.EnsureSuccessStatusCode();

                string responseBody = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<Dictionary<Project, int>>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine($"The error was caught:\n {e.Message}");
                return new Dictionary<Project, int>();
            }
        }

        public IEnumerable<STask> GetShortnameTasksByPerformerIdByUserId(int id)
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, url + "shortNameTasks/" + id);
                HttpResponseMessage response = Client.SendAsync(request).Result;
                response.EnsureSuccessStatusCode();

                string responseBody = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<IEnumerable<STask>> (responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine($"The error was caught:\n {e.Message}");
                return new List<STask>();
            }
        }

        public IEnumerable<(int Id, string Name)> GetFinishedTasksByUserId(int id, int year = 2019)
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, url + "finishedTasks/" + id + "/" + year);
                HttpResponseMessage response = Client.SendAsync(request).Result;
                response.EnsureSuccessStatusCode();

                string responseBody = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<IEnumerable<(int Id, string Name)>>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine($"The error was caught:\n {e.Message}");
                return new List<(int,string)>();
            }
        }

        public IEnumerable<(int id, string name, IEnumerable<User> userList)> GetSortedTeams()
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, url + "sortedTeams");
                HttpResponseMessage response = Client.SendAsync(request).Result;
                response.EnsureSuccessStatusCode();

                string responseBody = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<IEnumerable<(int id, string name, IEnumerable<User> userList)>>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine($"The error was caught:\n {e.Message}");
                return new List<(int, string, IEnumerable<User>)>();
            }
        }

        public IEnumerable<(Task user, IEnumerable<Task> tasks)> GetSortedUsersWithTasks()
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, url + "sortedBusyUsers");
                HttpResponseMessage response = Client.SendAsync(request).Result;
                response.EnsureSuccessStatusCode();

                string responseBody = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<IEnumerable<(Task user, IEnumerable<Task> tasks)>>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine($"The error was caught:\n {e.Message}");
                return new List<(Task, IEnumerable<Task>)>();
            }
        }

        public (User User, Project LastProject, int CanceledTasksCount, Task LongestTask, int LastProjectUserTasks) GetUserInfoById(int id)
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, url + "sortedBusyUsers/" + id);
                HttpResponseMessage response = Client.SendAsync(request).Result;
                response.EnsureSuccessStatusCode();

                string responseBody = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<(User User, Project LastProject, int CanceledTasksCount, Task LongestTask, int LastProjectUserTasks)>(responseBody);
            }
            catch (HttpRequestException e)
            {
                throw new Exception($"The error was caught:\n {e.Message}");
            }
        }

        public (Project Project, Task LongestTask, Task ShortestTask, int UsersCount) GetProjectInfoById(int id)
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, url + "projectInfo/" + id);
                HttpResponseMessage response = Client.SendAsync(request).Result;
                response.EnsureSuccessStatusCode();

                string responseBody = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<(Project Project, Task LongestTask, Task ShortestTask, int UsersCount)> (responseBody);
            }
            catch (HttpRequestException e)
            {
                throw new Exception($"The error was caught:\n {e.Message}");
            }
        }
        #endregion
    }
}
