﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ.Model.SpecialClasses
{
    class STask
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public State State { get; set; }
        public Project Project { get; set; }
        public User Performer { get; set; }
    }
}
