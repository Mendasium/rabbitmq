﻿using QueueService.Model;
using QueueServices.Abstractions;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace QueueServices.Services
{
    public class MessageConsumerScoped : IMessageConsumerScoped
    {
        private readonly Lazy<IMessageQueue> _messageQueue;
        private readonly Lazy<IMessageConsumer> _messageConsumer;

        private readonly MessageScopedSettings _messageScopedSettings;
        private readonly IConnectionFactory _connectionFactory;

        public MessageConsumerScoped(IConnectionFactory connectionFactory, MessageScopedSettings messageScopedSettings)
        {
            _messageScopedSettings = messageScopedSettings;
            _connectionFactory = connectionFactory;

            _messageConsumer = new Lazy<IMessageConsumer>(() => CreateMessageConsumer());
            _messageQueue = new Lazy<IMessageQueue>(() => new MessageQueue(connectionFactory, messageScopedSettings));
        }

        public IMessageConsumer MessageConsumer => _messageConsumer.Value;

        private IMessageQueue MessageQueue => _messageQueue.Value;

        private IMessageQueue CreateMessageQueue()
        {
            return new MessageQueue(_connectionFactory, _messageScopedSettings);
        }

        private IMessageConsumer CreateMessageConsumer()
        {
            return new MessageConsumer(new MessageConsumerSettings()
            {
                Channel = MessageQueue.Channel,
                QueueName = _messageScopedSettings.QueueName
            });
        }

        public void Dispose()
        {
            MessageQueue?.Dispose();
        }
    }
}
