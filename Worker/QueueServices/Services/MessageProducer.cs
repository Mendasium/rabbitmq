﻿using QueueService.Model;
using QueueServices.Abstractions;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace QueueServices.Services
{
    public class MessageProducer : IMessageProducer
    {
        private readonly MessageProducerSettings _messageProducerSettings;
        private readonly IBasicProperties _properties;

        public MessageProducer(MessageProducerSettings settings)
        {
            _messageProducerSettings = settings;

            _properties = _messageProducerSettings.Channel.CreateBasicProperties();
            _properties.Persistent = true;
        }

        public void Send(string message, string type = null)
        {
            if (!string.IsNullOrEmpty(type))
            {
                _properties.Type = type;
            }

            var body = Encoding.UTF8.GetBytes(message);
            _messageProducerSettings.Channel.BasicPublish(_messageProducerSettings.PublicationAddress, _properties, body);
        }

        public void SendTyped(string message, Type type)
        {
            Send(message, type.AssemblyQualifiedName);
        }
    }
}
