﻿using Worker.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

namespace Worker.Writer
{
    public class MessageWriterReaderToFile : Interfaces.IWriterReader<Message>
    {
        private string path;
        public MessageWriterReaderToFile(string path = null)
        {
            if (path == null)
                this.path = Environment.CurrentDirectory;
            else
                this.path = path;
        }

        public void Write(Message message)
        {
            if (!File.Exists(path + "/Messages.log"))
                File.Create(path + "/Messages.log");
            Thread.Sleep(100);

            using (StreamWriter write = new StreamWriter(path + "/Messages.log", true))
            {
                string resultLine = message.Id + "$$" + message.MessageBody + "$$" + message.DateTimeCreated.ToString();
                write.WriteLine(resultLine);
            }
        }
        public IEnumerable<Message> ReadAll()
        {
            if (!File.Exists(path + "/Messages.log"))
                return new List<Message>();
            List<Message> history = new List<Message>();
            using (StreamReader sr = new StreamReader(path + "/Messages.log"))
            {
                string line = sr.ReadLine();
                while (line != null)
                {
                    int currentPos = 0;
                    int messageId = int.Parse(line.Substring(0, line.IndexOf("$$", currentPos)));
                    currentPos = line.IndexOf("$$", currentPos) + 2;
                    string messageBody = line.Substring(currentPos, line.IndexOf("$$", currentPos) - currentPos);
                    currentPos = line.IndexOf("$$", currentPos) + 2;
                    DateTime dateTime = DateTime.Parse(line.Substring(currentPos, line.Length - currentPos));
                    history.Add(new Message()
                    {
                        DateTimeCreated = dateTime,
                        Id = messageId,
                        MessageBody = messageBody
                    });
                    line = sr.ReadLine();
                }
            }
            return history;
        }
    }
}
