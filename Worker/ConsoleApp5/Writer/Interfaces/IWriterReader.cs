﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Worker.Writer.Interfaces
{
    public interface IWriterReader<T>
    {
        void Write(T data);
        IEnumerable<T> ReadAll();
    }
}
