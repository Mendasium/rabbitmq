﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Worker.Model
{
    public class Message
    {
        static int id = 0;
        public int Id { get; set; }
        public DateTime DateTimeCreated { get; set; }

        public string MessageBody { get; set; }

        public Message()
        {
            id++;
            Id = id;
        }
    }
}
