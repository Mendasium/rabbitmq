﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using QueueServices.Abstractions;
using QueueServices.Services;
using Worker.Model;
using Worker.Writer;
using Worker.Writer.Interfaces;

namespace Worker
{
    class Program
    {
        static void Main(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(AppContext.BaseDirectory).FullName)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .Build();

            var serviceProvider = new ServiceCollection()
                .AddSingleton<RabbitMQ.Client.IConnectionFactory>(x => new ConnectionFactory(new Uri(configuration.GetConnectionString("RabbitMQ"))))
                .AddScoped<IMessageProducer, MessageProducer>()
                .AddScoped<IMessageProducerScoped, MessageProducerScoped>()
                .AddSingleton<IMessageProducerScopedFactory, MessageProducerScopedFactory>()
                .AddScoped<IMessageConsumer, MessageConsumer>()
                .AddScoped<IMessageConsumerScoped, MessageConsumerScoped>()
                .AddSingleton<IMessageConsumerScopedFactory, MessageConsumerScopedFactory>()
                .AddScoped<QueueService>()
                .AddScoped<IWriterReader<Message>, MessageWriterReaderToFile>()
               .BuildServiceProvider();

            var queue = serviceProvider.GetService<QueueService>();

            WorkerInterface workerInterface = new WorkerInterface(queue);
            
            Start();
        }
        public static void Start()
        {
            Console.ReadLine();
        }
    }
}
