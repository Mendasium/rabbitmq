﻿using System;
using Microsoft.Extensions.DependencyInjection;
using QueueServices.Abstractions;
using QueueServices.Services;

namespace Worker
{
    public class Program
    {
        public static void Main()//string[] args
        {
            var serviceProvider = new ServiceCollection()
                .AddSingleton<RabbitMQ.Client.IConnectionFactory>(x => new ConnectionFactory(new Uri("amqp://guest:guest@localhost:5673")))
                .AddScoped<IMessageProducer, MessageProducer>()
                .AddScoped<IMessageProducerScoped, MessageProducerScoped>()
                .AddSingleton<IMessageProducerScopedFactory, MessageProducerScopedFactory>()
                .AddScoped<IMessageConsumer, MessageConsumer>()
                .AddScoped<IMessageConsumerScoped, MessageConsumerScoped>()
                .AddSingleton<IMessageConsumerScopedFactory, MessageConsumerScopedFactory>()
               .BuildServiceProvider();

            Console.WriteLine("asdfa");
            Console.ReadKey();
            //configure console logging
            //serviceProvider
            //    .GetService<ILoggerFactory>()
            //    .AddConsole(LogLevel.Debug);

            //var logger = serviceProvider.GetService<ILoggerFactory>()
            //    .CreateLogger<Program>();
            //logger.LogDebug("Starting application");

            //logger.LogDebug("All done!");
            Console.WriteLine("1312");

        }
        public static void Start()
        {
            QueueService queue = new QueueService();
            Console.ReadLine();
        }
    }
}
