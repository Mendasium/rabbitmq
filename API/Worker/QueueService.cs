﻿using QueueService.Model;
using QueueServices.Abstractions;
using QueueServices.Services;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace Worker
{
    class QueueService
    {
        //IMessageProducerScoped _messageProducerScope;
        //IMessageConsumerScoped _messageConsumerScope;

        //public QueueService(IMessageProducerScopedFactory messageProducerScope, IMessageConsumerScopedFactory messageConsumerScope)
        //{

        //    _messageConsumerScope = messageConsumerScope.Open(new MessageScopedSettings()
        //    {
        //        ExchangeName = "ClientExchange",
        //        ExchangeType = ExchangeType.Direct,
        //        QueueName = "responsesQueue",
        //        RoutingKey = "response"
        //    });


        //    _messageProducerScope = messageProducerScope.Open(new MessageScopedSettings()
        //    {
        //        ExchangeType = ExchangeType.Topic,
        //        ExchangeName = "ServerExchange",
        //        QueueName = "requestQueue",
        //        RoutingKey = "request"
        //    });

        //    _messageConsumerScope.MessageConsumer.Received += GetValue;
        //}

        private void GetValue(object sender, BasicDeliverEventArgs e)
        {
            Console.WriteLine(Encoding.UTF8.GetString(e.Body));
        }

        public QueueService()
        {
            var factory = new RabbitMQ.Client.ConnectionFactory() { Uri = new Uri("amqp://guest:guest@localhost:5673") };

            MessageConsumer Consumer = new MessageConsumer(new MessageConsumerSettings()
            {
                Channel = factory.CreateConnection().CreateModel(),
                QueueName = "requestQueue"
            });

            Consumer.Received += GetValue;

            
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare(exchange: "logs", type: "fanout");

                var queueName = channel.QueueDeclare().QueueName;

                channel.QueueBind(queue: queueName,
                              exchange: "logs",
                              routingKey: "");

                //channel.QueueDeclare(queue: queueName,
                //                     durable: true,
                //                     exclusive: false,
                //                     autoDelete: false,
                //                     arguments: null);



                channel.BasicQos(0, 1, false); //Посылать только по одному сообщению на consumer


                var consumer = new EventingBasicConsumer(channel);

                consumer.Received += (sender, e) =>
                {
                    Console.WriteLine("Message was received");
                    Console.WriteLine(sender.ToString());
                    Console.WriteLine(e.ToString());
                    Console.WriteLine(Encoding.UTF8.GetString(e.Body));

                    channel.BasicAck(e.DeliveryTag, false);
                };
                channel.BasicConsume(queue: queueName,
                                 autoAck: false,
                                 consumer: consumer);

                Console.ReadLine();
            }
        }
    }
}
