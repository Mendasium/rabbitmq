﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedServices.Writer.Interfaces
{
    public interface IWriterReader<T>
    {
        void Write(T data);
        IEnumerable<T> ReadAll(string path = null);
        IEnumerable<T> ReadAll();
    }
}
