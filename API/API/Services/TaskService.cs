﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskProject.Model;
using TaskProject.Repositories;
using TaskProject.Services.ServicesAbstraction;

namespace TaskProject.Services
{
    public class TaskService : IService<Task>
    {
        IRepository<Task> _tasks;
        IRepository<User> _users;
        IRepository<Project> _projects;
        IRepository<State> _states;
        IQueueService _queueService;

        public TaskService(IRepository<Task> tasks, IRepository<User> users, IRepository<Project> projects, IRepository<State> states, IQueueService queueService)
        {
            _tasks = tasks;
            _users = users;
            _projects = projects;
            _states = states;
        }

        public void Create(Task entity)
        {
            if (entity.ProjectId != 0)
            {
                var project = _projects.Get(new Func<Project, bool>(x => x.Id == entity.ProjectId)).FirstOrDefault();
                entity.Project = project ?? throw new Exception("Such project doesnt exist");
            }
            else
                throw new Exception("Project must be added");

            if (entity.PerformerId != 0)
            {
                var performer = _users.Get(new Func<User, bool>(x => x.Id == entity.PerformerId)).FirstOrDefault();
                entity.Performer = performer ?? throw new Exception("Such performer doesnt exist");
            }
            else
                throw new Exception("Performer must be added");
            
            if(entity.NewStateId != 0)
            {
                var state = _states.Get(new Func<State, bool>(x => x.Id == entity.NewStateId)).FirstOrDefault();
                entity.State = state ?? throw new Exception("Such state doesnt exist");
            }
            else
                throw new Exception("State must be added");

            entity.CreatedAt = DateTime.Now;
            entity.NewStateId = entity.PerformerId = entity.ProjectId = 0;
            _tasks.Create(entity);
            _queueService.Send($"Task {entity.ToString()} was created");
        }
        

        public void Delete(Task entity)
        {
            _tasks.Delete(entity);
            _queueService.Send($"Task {entity.ToString()} was removed");
        }

        public void Delete(int id)
        {
            _tasks.Delete(id);
            _queueService.Send($"Task {id} was removed");
        }

        public IEnumerable<Task> GetEntities(Func<Task, bool> filter = null)
        {
            _queueService.Send($"Getting all Tasks " + (filter == null ? "" : " with filter"));
            return _tasks.Get(filter);
        }

        public void Update(Task entity)
        {
            if (entity.NewStateId != 0)
            {
                var state = _states.Get(x => x.Id == entity.NewStateId).FirstOrDefault();
                if(state != null)
                    entity.State = state;
            }

            var task = _tasks.Get(x => x.Id == entity.Id).FirstOrDefault();
            if (task != null)
                entity.CreatedAt = task.CreatedAt;

            entity.NewStateId = entity.PerformerId = entity.ProjectId = 0;

            _tasks.Update(entity);
            _queueService.Send($"Task {entity.ToString()} was updated");
        }
    }
}
