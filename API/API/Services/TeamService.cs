﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskProject.Model;
using TaskProject.Repositories;
using TaskProject.Services.ServicesAbstraction;

namespace TaskProject.Services
{
    public class TeamService : IService<Team>
    {
        IRepository<Team> _states;
        IQueueService _queueService;

        public TeamService(IRepository<Team> states, IQueueService queueService)
        {
            _states = states;
        }

        public void Create(Team entity)
        {
            entity.CreatedAt = DateTime.Now;
            _states.Create(entity);
            _queueService.Send($"Team {entity.ToString()} was created");
        }

        public void Delete(Team entity)
        {
            _states.Delete(entity);
            _queueService.Send($"Team {entity.ToString()} was removed");
        }

        public void Delete(int id)
        {
            _states.Delete(id);
            _queueService.Send($"Team {id} was removed");
        }

        public IEnumerable<Team> GetEntities(Func<Team, bool> filter = null)
        {
            _queueService.Send($"Getting all Teams " + (filter == null ? "" : " with filter"));
            return _states.Get(filter);
        }

        public void Update(Team entity)
        {
            var state = _states.Get(x => x.Id == entity.Id).FirstOrDefault();
            if(state != null)
                entity.CreatedAt = state.CreatedAt;
            _states.Update(entity);
            _queueService.Send($"Team {entity.ToString()} was updated");
        }
    }
}
