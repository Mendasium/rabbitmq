﻿using Microsoft.AspNetCore.SignalR;
using QueueService.Model;
using QueueServices.Abstractions;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskProject.Services.ServicesAbstraction;

namespace TaskProject.Services
{
    public class QueueService : IQueueService
    {
        private readonly IMessageProducerScoped _messageProducerScope;
        private readonly IMessageConsumerScoped _messageConsumerScope;
        private readonly IHubContext<AnswerHub> _hubContext;

        public QueueService(IMessageProducerScopedFactory messageProducerScope, IMessageConsumerScopedFactory messageConsumerScope, IHubContext<AnswerHub> hubContext)
        {
            _hubContext = hubContext;

            _messageConsumerScope = messageConsumerScope.Open(new MessageScopedSettings()
            {
                ExchangeName = "ClientExchange",
                ExchangeType = ExchangeType.Direct,
                QueueName = "responsesQueue",
                RoutingKey = "response"
            });

            _messageProducerScope = messageProducerScope.Open(new MessageScopedSettings()
            {
                ExchangeType = ExchangeType.Topic,
                ExchangeName = "ServerExchange",
                QueueName = "requestQueue",
                RoutingKey = "request"
            });

            _messageConsumerScope.MessageConsumer.Connect();
            _messageConsumerScope.MessageConsumer.Received += GetValue;
        }

        public void GetValue(object sender, BasicDeliverEventArgs e)
        {
            try
            {
                string result = Encoding.UTF8.GetString(e.Body);
                _hubContext.Clients.All.SendAsync("displayMessage", result);
                _messageConsumerScope.MessageConsumer.SetAcknowledge(e.DeliveryTag, true);
            }
            catch (Exception exc)
            {
                _hubContext.Clients.All.SendAsync("displayMessage", "Something wrong on server");
                _messageConsumerScope.MessageConsumer.SetAcknowledge(e.DeliveryTag, false);
            }
        }

        public void Send(string message)
        {
            _messageProducerScope.MessageProducer.Send(message);
        }
    }
}
