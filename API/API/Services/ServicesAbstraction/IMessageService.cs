﻿using SharedServices.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskProject.Services.ServicesAbstraction
{
    public interface IMessageService
    {
        IEnumerable<Message> ReadAll(); 
    }
}
