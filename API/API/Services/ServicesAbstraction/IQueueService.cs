﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskProject.Services.ServicesAbstraction
{
    public interface IQueueService
    {
        void Send(string message);
    }
}
