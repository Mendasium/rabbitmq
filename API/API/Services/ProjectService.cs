﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskProject.Model;
using TaskProject.Repositories;
using TaskProject.Services.ServicesAbstraction;

namespace TaskProject.Services
{
    public class ProjectService : IService<Project>
    {
        IRepository<Project> _projects;
        IRepository<User> _users;
        IRepository<Team> _teams;
        IQueueService _queueService;

        public ProjectService(IRepository<Project> projects, IRepository<User> users, IRepository<Team> teams, IQueueService queueService)
        {
            _users = users;
            _projects = projects;
            _teams = teams;
        }

        public void Create(Project entity)
        {
            if (entity.AuthorId == 0)
                throw new Exception("The project can not has no author");
            var team = _users.Get(new Func<User, bool>(x => x.Id == entity.AuthorId)).FirstOrDefault();
            entity.Author = team ?? throw new Exception("Such Author doesnt exist");

            if (entity.NewTeamId != 0)
            {
                entity.Team = _teams.Get(new Func<Team, bool>(x => x.Id == entity.NewTeamId)).FirstOrDefault();
            }

            entity.CreatedAt = DateTime.Now;
            entity.AuthorId = entity.NewTeamId = 0;
            _projects.Create(entity);
            _queueService.Send($"Project {entity} was created");
        }

        public void Delete(Project entity)
        {
            _projects.Delete(entity);
            _queueService.Send($"Project {entity} was removed");
        }

        public void Delete(int id)
        {
            _projects.Delete(id);
            _queueService.Send($"Project {id} was removed");
        }

        public IEnumerable<Project> GetEntities(Func<Project, bool> filter = null)
        {
            _queueService.Send($"Getting all Entities " + (filter == null ? "" : " with filter"));
            return _projects.Get(filter);
        }

        public void Update(Project entity)
        {
            if (entity.NewTeamId != 0)
            {
                var team = _teams.Get(new Func<Team, bool>(x => x.Id == entity.NewTeamId)).FirstOrDefault();
                if (team != null)
                    entity.Team = team;
            }

            var project = _projects.Get(x => x.Id == entity.Id).FirstOrDefault();
            if(project != null)
                entity.CreatedAt = project.CreatedAt;
            entity.AuthorId = entity.NewTeamId = 0;
            _projects.Update(entity);
            _queueService.Send($"Project {entity} was updated");
        }
    }
}
