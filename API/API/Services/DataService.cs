﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using TaskProject.Model;
using TaskProject.Model.DTOModel;
using TaskProject.Repositories;

namespace TaskProject.Services
{
    public class DataService
    {
        IRepository<State> _statesRepository;
        IRepository<Project> _projectRepository;
        IRepository<Task> _taskRepository;
        IRepository<Team> _teamRepository;
        IRepository<User> _userRepository;
        IMapper _mapper;
        public DataService(IRepository<State> statesRepository, IRepository<Project> projectRepository, IRepository<Task> taskRepository, IRepository<Team> teamRepository, IRepository<User> userRepository, IMapper mapper)
        {
            _mapper = mapper;
            _statesRepository = statesRepository;
            _projectRepository = projectRepository;
            _taskRepository = taskRepository;
            _teamRepository = teamRepository;
            _userRepository = userRepository;
        }

        public Dictionary<DTOProject, int> GetProjectTasksCountByUserId(int id)
        {
            var tasks = _taskRepository.Get();

            return tasks.Where(t => t.PerformerId == id)
                .GroupBy(x => x.Project)
                .ToDictionary(x => _mapper.Map<Project, DTOProject>(x.Key), x => x.Count());
        }
        
        public IEnumerable<Task> GetShortnameTasksByPerformerIdByUserId(int id)
        {
            var tasks = _taskRepository.Get();
            return tasks.Where(t => t.Name.Length < 45 && t.Performer.Id == id);
        }
        
        public IEnumerable<(int Id, string Name)> GetFinishedTasksByUserId(int id, int year = 2019)
        {
            var projects = _taskRepository.Get();

            return projects.Where(t => t.Performer.Id == id && t.FinishedAt.Year == year && t.State.Value.Equals("finished"))
                .Select(x => (Id: x.Id, Name: x.Name));
        }
        
        public IEnumerable<(int id, string name, IEnumerable<DTOUser> userList)> GetSortedTeams()
        {//Команд в которых все учасники старше 12 лет нету
            var users = _userRepository.Get();

            return users.GroupBy(u => u.Team)
                .Where(u => u.All(x => DateTime.Now.Year - x.Birthday.Year >= 12))
                .Select(t =>
                    (id: t.Key.Id,
                        name: t.Key.Name,
                    userList: _mapper.Map<IEnumerable<User>, IEnumerable<DTOUser>>(t.OrderByDescending(x => x.RegisteredAt)))
                    );
        }
        
        public IEnumerable<(DTOUser user, IEnumerable<DTOTask> tasks)> GetSortedUsersWithTasks()
        {
            var tasks = _taskRepository.Get();

            return tasks.GroupBy(x => x.Performer.Id)
                .Select(x => (
                        Performer: _mapper.Map<User, DTOUser>(x.FirstOrDefault().Performer),
                        Tasks: _mapper.Map<IEnumerable<Task>, IEnumerable<DTOTask>>(x.OrderByDescending(y => y.Name.Length).ToList())))
                .OrderBy(x => x.Performer.FirstName);
        }
        
        public (DTOUser User, DTOProject LastProject, int CanceledTasksCount, DTOTask LongestTask, int LastProjectUserTasks) GetUserInfoById(int id)
        {
            var user = _userRepository.Get(x => x.Id == id).FirstOrDefault();
            var lastProject = _projectRepository.Get().OrderBy(x => x.CreatedAt).LastOrDefault();
            var tasks = _taskRepository.Get(t => t.Performer.Id == id);

            return
                (
                    User: _mapper.Map<User, DTOUser>(user),
                    LastProject: _mapper.Map<Project, DTOProject>(lastProject),
                    CanceledTasksCount: tasks.Count(x => x.State.Value == "Canceled" || x.State.Value == "Started"),
                    LongestTask: _mapper.Map<Task, DTOTask>(tasks.OrderBy(x => x.FinishedAt - x.CreatedAt).FirstOrDefault()),
                    LastProjectUserTasks: tasks.Count(x => x.Project.Id == lastProject.Id)
                );
        }
        
        public (DTOProject Project, DTOTask LongestTask, DTOTask ShortestTask, int UsersCount) GetProjectInfoById(int id)
        {
            var project = _projectRepository.Get(x => x.Id == id).FirstOrDefault();
            var projectTasks = _taskRepository.Get(x => x.Project.Id == id);
            var users = _projectRepository.Get();

            return
                (
                    Project: _mapper.Map<Project, DTOProject>(project),
                    LongestTask: _mapper.Map<Task, DTOTask>(projectTasks.OrderByDescending(t => t.Description).FirstOrDefault()),
                    ShortestTask: _mapper.Map<Task, DTOTask>(projectTasks.OrderBy(t => t.Name).FirstOrDefault()),
                    UsersCount: (projectTasks.Count() < 3 || project.Description.Length > 25 ?
                                                users.Where(x => x.Team.Id == project.Team.Id).Count() : -1)
                );
        }
    }
}
