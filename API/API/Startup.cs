﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using QueueServices.Abstractions;
using QueueServices.Services;
using RabbitMQ.Client;
using SharedServices.Model;
using SharedServices.Writer;
using SharedServices.Writer.Interfaces;
using TaskProject.Model;
using TaskProject.Model.DTOModel;
using TaskProject.Repositories;
using TaskProject.Services;
using TaskProject.Services.ServicesAbstraction;

namespace TaskProject
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddTransient<IService<State>, StateService>();
            services.AddTransient<IService<User>, UserService>();
            services.AddTransient<IService<Project>, ProjectService>();
            services.AddTransient<IService<Task>, TaskService>();
            services.AddTransient<IService<Team>, TeamService>();

            services.AddSingleton<IWriterReader<Message>>(x => new MessageWriterReaderToFile(Configuration.GetSection("MessageLogPath").Value));
            services.AddTransient<IMessageService, MessageService>();

            services.AddSingleton<IRepository<State>, Repository<State>>();
            services.AddSingleton<IRepository<User>, Repository<User>>();
            services.AddSingleton<IRepository<Team>, Repository<Team>>();
            services.AddSingleton<IRepository<Task>, Repository<Task>>();
            services.AddSingleton<IRepository<Project>, Repository<Project>>();

            services.AddSingleton<IQueueService, Services.QueueService>();
            services.AddScoped<IMessageQueue, MessageQueue>();
            services.AddSingleton<IConnectionFactory>(x => new QueueServices.Services.ConnectionFactory(new Uri(Configuration.GetConnectionString("RabbitMQ"))));

            services.AddScoped<IMessageProducer, MessageProducer>();
            services.AddScoped<IMessageProducerScoped, MessageProducerScoped>();
            services.AddSingleton<IMessageProducerScopedFactory, MessageProducerScopedFactory>();

            services.AddScoped<IMessageConsumer, MessageConsumer>();
            services.AddScoped<IMessageConsumerScoped, MessageConsumerScoped>();
            services.AddSingleton<IMessageConsumerScopedFactory, MessageConsumerScopedFactory>();

            var mapper = MapperConfiguration().CreateMapper();

            services.AddScoped(_ => mapper);

            services.AddSignalR();
        }


        public MapperConfiguration MapperConfiguration()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<DTOState, State>();
                cfg.CreateMap<State, DTOState>();


                cfg.CreateMap<DTOUser, User>()
                    .ForMember(i => i.NewTeamId, l => l.MapFrom(i => i.TeamId))
                    .ForMember(i => i.RegisteredAt, opt => opt.Ignore())
                    .ForMember(i => i.Team, opt => opt.Ignore());
                cfg.CreateMap<User, DTOUser>()
                    .ForMember(i => i.TeamId, l => l.MapFrom(i => i.Team.Id));

                cfg.CreateMap<DTOTeam, Team>()
                    .ForMember(i => i.CreatedAt, opt => opt.Ignore());
                cfg.CreateMap<Team, DTOTeam>();

                cfg.CreateMap<DTOProject, Project>()
                    .ForMember(i => i.NewTeamId, opt => opt.MapFrom(l => l.TeamId))
                    .ForMember(i => i.AuthorId, opt => opt.MapFrom(l => l.AuthorId))
                    .ForMember(i => i.CreatedAt, opt => opt.Ignore())
                    .ForMember(i => i.Team, opt => opt.Ignore())
                    .ForMember(i => i.Author, opt => opt.Ignore());
                cfg.CreateMap<Project, DTOProject>()
                    .ForMember(i => i.TeamId, opt => opt.MapFrom(l => l.NewTeamId));

                cfg.CreateMap<DTOTask, Task>()
                    .ForMember(i => i.CreatedAt, l => l.Ignore())
                    .ForMember(i => i.Project, l => l.Ignore())
                    .ForMember(i => i.Performer, l => l.Ignore())
                    .ForMember(i => i.State, l => l.Ignore())
                    .ForMember(i => i.NewStateId, opt => opt.MapFrom(i => i.StateId));
                cfg.CreateMap<Task, DTOTask>()
                    .ForMember(i => i.PerformerId, opt => opt.MapFrom(i => i.Performer.Id))
                    .ForMember(i => i.StateId, opt => opt.MapFrom(i => i.State.Id))
                    .ForMember(i => i.ProjectId, opt => opt.MapFrom(i => i.Project.Id));
                });
            return config;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            
           // app.UseCors(builder => builder.AllowAnyHeader().AllowAnyMethod().AllowCredentials().WithOrigins("127.0.0.1:44343"));
            app.UseSignalR(routes =>
            {
                routes.MapHub<AnswerHub>("/answers");
            });

            app.UseHttpsRedirection();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });
        }
    }
}
