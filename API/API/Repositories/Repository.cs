﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TaskProject.Model;

namespace TaskProject.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        private readonly List<TEntity> _entities;

        public Repository()
        {
            _entities = new List<TEntity>();
        }

        public void Create(TEntity entity)
        {
            entity.Id = _entities.Select(x => x.Id).ToArray().GenerateUniqueRandomNumber();
            _entities.Add(entity);
        }

        public void Delete(TEntity entity)
        {
            var e = _entities.FirstOrDefault(x => x.Id == entity.Id);
            if (e != null)
                _entities.Remove(e);
        }

        public void Delete(int id)
        {
            var e = _entities.FirstOrDefault(x => x.Id == id);
            if (e != null)
                _entities.Remove(e);
        }

        public IEnumerable<TEntity> Get(Func<TEntity, bool> filter = null)
        {
            if(filter != null)
                return _entities.Where(filter);
            return _entities;
        }

        public void Update(TEntity entity)
        {
            TEntity e = _entities.FirstOrDefault(x => x.Id == entity.Id);
            e.Update(entity);
            //if(e != null)
            //{
            //    _entities.Remove(e);
            //    _entities.Add(entity);
            //}
        }
    }
}
