﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskProject.Repositories
{
    public static class IntGenerator
    {
        private static readonly Random random = new Random(DateTime.Now.Millisecond);

        public static int GetNewRandomNumber(int maxValue = 1000)
        {
            return random.Next(maxValue);
        }
        public static int GenerateUniqueRandomNumber(this int[] numbers, int maxValue = 1000)
        {
            int number = random.Next(maxValue);
            while (numbers.Contains(number))
                number = random.Next();
            return number;
        }
    }
}
