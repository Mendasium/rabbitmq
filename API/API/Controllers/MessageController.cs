﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using TaskProject.Services;
using TaskProject.Model;
using AutoMapper;
using TaskProject.Model.DTOModel;
using System.Linq;
using TaskProject.Services.ServicesAbstraction;

namespace TaskProject.Controllers
{
    [Route("api/[controller]")]
    public class MessagesController : ControllerBase
    {
        IMessageService _messageService;

        public MessagesController(IMessageService messageService)
        {
            _messageService = messageService;
        }

        [HttpGet]
        public IEnumerable<string> GetAll()
        {
            return _messageService.ReadAll().Select(x => x.ToString());
        }
    }
}
