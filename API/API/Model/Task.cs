﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskProject.Model
{
    public class Task : Entity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public State State { get; set; }
        public Project Project { get; set; }
        public User Performer { get; set; }

        public int NewStateId { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }

        public Task() { }

        public override string ToString()
        {
            return $"{Id} - {Name} - {Description}";
        }

        public override void Update(Entity entity)
        {
            if (entity is Task task)
            {
                if (!string.IsNullOrEmpty(task.Name) && task.Name.Length > 3 && task.Name != Name)
                    this.Name = task.Name;
                if (!string.IsNullOrEmpty(task.Description) && task.Description.Length > 3 && task.Description != Description)
                    this.Description = task.Description;
                if (FinishedAt != task.FinishedAt && task.FinishedAt != new DateTime())
                    this.FinishedAt = task.FinishedAt;
                this.State = task.State;
                if(task.Project != null)
                    this.Project = task.Project;
                if (task.Performer != null)
                    this.Performer = task.Performer;
                if (task.State != null)
                    this.State = task.State;
            }
            else
            {
                throw new FormatException("You need to use task entity here");
            }
        }
    }
}
