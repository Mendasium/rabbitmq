﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskProject.Model.DTOModel
{
    public class DTOTask
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime FinishedAt { get; set; }

        public int StateId { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
    }
}
