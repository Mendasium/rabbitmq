﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskProject.Model.DTOModel
{
    public class DTOState
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
