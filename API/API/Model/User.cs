﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TaskProject.Model
{
    public class User : Entity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime RegisteredAt { get; set; }
        public Team Team { get; set; }

        public int NewTeamId { get; set; }

        public User() { }
        
        public override string ToString()
        {
            return $"{Id} - {FirstName} {LastName}";
        }

        public override void Update(Entity entity)
        {
            if (entity is User user)
            {
                if (!string.IsNullOrEmpty(user.FirstName) && user.FirstName.Length > 3 && user.FirstName != FirstName)
                    this.FirstName = user.FirstName;
                if (!string.IsNullOrEmpty(user.LastName) && user.LastName.Length > 3 && user.LastName != LastName)
                    this.LastName = user.LastName;
                if (!string.IsNullOrEmpty(user.Email) && user.Email.Length > 3 && user.Email != Email)
                    this.Email = user.Email;
                if (user.Team != null)
                    this.Team = user.Team;
            }
            else
            {
                throw new FormatException("You need to use user entity here");
            }
        }
    }
}
